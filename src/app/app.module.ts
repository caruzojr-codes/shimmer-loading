import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NgxShimmerLoadingModule } from  'ngx-shimmer-loading';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxShimmerLoadingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
