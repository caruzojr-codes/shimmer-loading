import { Component } from '@angular/core';

import { UsuariosService } from './shared/services/usuarios.service';
import { Usuario } from './shared/models/usuario.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public isShimmerLoading = true;
  public listaUsuarios: Usuario[] = [];

  constructor(
    private usuariosService: UsuariosService
  ) { }

  ngOnInit() {
    this.getUsuarios();
  }

  getUsuarios() {
    return this.usuariosService.getUsuarios()
      .subscribe((result: Usuario[]) => {
        this.listaUsuarios = result
        this.isShimmerLoading = false;
        console.log(this.listaUsuarios);
      },
      error => console.log(error))
  }

}
