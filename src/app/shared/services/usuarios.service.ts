import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, delay } from 'rxjs/operators';

import { Usuario } from './../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private urlAPI: string = "http://localhost:3000";

  constructor(
    private httpClient: HttpClient
  ) { }

  getUsuarios(): Observable<Usuario[]> {
    return this.httpClient.get<Usuario[]>(
			this.urlAPI + `/usuarios`
		).pipe(
      delay(5000),
			catchError(error => {
				return throwError(Object.keys(error));
			})
		)
  }

}
